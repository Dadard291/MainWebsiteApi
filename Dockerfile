FROM python:latest

WORKDIR /usr/src/app

ARG MYSQL_MAIN_PASSWORD
ARG JWT_SECRET

COPY requirements.txt ./

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

RUN apt-get update
RUN apt-get install -y pandoc

# change default favicon
COPY ./static/static/* /usr/local/lib/python3.8/site-packages/flask_restplus/static/
COPY ./static/templates/* /usr/local/lib/python3.8/site-packages/flask_restplus/templates/

ENV MYSQL_MAIN_PASSWORD=$MYSQL_MAIN_PASSWORD
ENV JWT_SECRET=$JWT_SECRET
ENV API_ENV=PROD

COPY . .

USER root
RUN chmod a+w ./logs

CMD [ "python", "./app.py" ]
