from MainWebsiteApi.common.common import hash_password, encode_password, generate_profile_key
from MainWebsiteApi.common.exceptions import DalException, BusinessException
from MainWebsiteApi.dal.layers.profile_dal import ProfileDal


class ProfileManager:
    def __init__(self):
        self.dal = ProfileDal()

    def create_profile(self, username, password):
        try:
            profile_key = generate_profile_key()
            hashed_password = hash_password(password)
            return self.dal.create_profile(username, hashed_password, profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def delete_profile(self, username, password):
        try:
            encoded_password = encode_password(password)
            return self.dal.delete_profile(username, encoded_password)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def get_profile(self, username, password):
        try:
            encoded_password = encode_password(password)
            return self.dal.get_profile(username, encoded_password)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def update_profile(self, username, password, new_password):
        try:
            encoded_password = encode_password(password)
            new_hashed_password = hash_password(new_password)
            return self.dal.update_profile(username, encoded_password, new_hashed_password)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def regenerate_profile_key(self, username, password):
        try:
            encoded_password = encode_password(password)
            new_profile_key = generate_profile_key()
            return self.dal.regenerate_profile_key(username, encoded_password, new_profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def get_profile_infos(self, profile_key):
        try:
            return self.dal.get_profile_infos(profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def create_jwt(self, profile_key):
        try:
            return self.dal.create_jwt(profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))
