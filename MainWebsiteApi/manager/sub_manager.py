from MainWebsiteApi.common.exceptions import DalException, BusinessException
from MainWebsiteApi.dal.layers.sub_dal import SubDal


class SubManager:
    def __init__(self):
        self.dal = SubDal()

    def create_sub(self, api_name, profile_key):
        try:
            return self.dal.create_sub(api_name, profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def delete_sub(self, api_name, profile_key):
        try:
            return self.dal.delete_sub(api_name, profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def get_sub(self, api_name, profile_key):
        try:
            return self.dal.get_sub(api_name, profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def list_subs(self, profile_key):
        try:
            return self.dal.list_subs(profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))
