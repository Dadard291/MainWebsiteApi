import pypandoc as pypandoc

from MainWebsiteApi.common.exceptions import DalException, BusinessException
from MainWebsiteApi.dal.layers.api_dal import ApiDal


class ApiManager:
    def __init__(self):
        self.dal = ApiDal()

    def ping(self, api_name):
        try:
            return self.dal.ping(api_name)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def register_api(self, api_object, profile_key):
        try:
            return self.dal.register_api(api_object, profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def deregister_api(self, api_name,profile_key):
        try:
            return self.dal.deregister_api(api_name, profile_key)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def get_api(self, api_name):
        try:
            return self.dal.get_api(api_name)
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def list_api(self):
        try:
            return self.dal.list_apis()
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))

    def retrieve_long_description(self, api_name):
        try:
            raw_des = self.dal.retrieve_long_description(api_name)
            html_des = pypandoc.convert_text(raw_des, 'html', format='rst')
            return html_des
        except DalException as de:
            raise BusinessException(de.msg)
        except Exception as e:
            raise BusinessException(str(e))
