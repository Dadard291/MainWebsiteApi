from MainWebsiteApi.dal.repository.connector import Connector


class ApiEntity(Connector.db.Model):
    __tablename__ = 'apis'

    # overview
    name = Connector.db.Column(Connector.db.String(30), unique=True, primary_key=True)
    description = Connector.db.Column(Connector.db.String(300))
    port = Connector.db.Column(Connector.db.Integer())
    creation_date = Connector.db.Column(Connector.db.Date())

    # details
    url_badge_build = Connector.db.Column(Connector.db.String(100))
    url_badge_package = Connector.db.Column(Connector.db.String(100))
    container_image_registry = Connector.db.Column(Connector.db.String(50))
    long_description = Connector.db.Column(Connector.db.String(100))
    api_documentation = Connector.db.Column(Connector.db.String(100))

    subscriptions = Connector.db.relationship('SubscriptionEntity', backref=__tablename__, lazy=True)

    def __init__(self, name=None,
                 description=None,
                 port=None,
                 creation_date=None,
                 url_badge_build=None,
                 url_badge_package=None,
                 container_image_registry=None,
                 long_description=None,
                 api_documentation=None
                 ):
        self.name = name
        self.description = description
        self.port = port
        self.creation_date = creation_date
        self.url_badge_build = url_badge_build
        self.url_badge_package = url_badge_package
        self.container_image_registry = container_image_registry
        self.long_description = long_description
        self.api_documentation = api_documentation

    def __repr__(self):
        return '<API {name}>'.format(name=self.name)

    def overview_to_dict(self):
        return dict(
            name=self.name,
            description=self.description,
            port=self.port,
            creation_date=str(self.creation_date)
        )

    def to_dict(self):
        base = self.overview_to_dict()
        base.update(dict(
            url_badge_build=self.url_badge_build,
            url_badge_package=self.url_badge_package,
            container_image_registry=self.container_image_registry,
            long_description=self.long_description,
            api_documentation=self.api_documentation
        ))

        return base


class SubscriptionEntity(Connector.db.Model):
    __tablename__ = 'subscriptions'
    sub_id = Connector.db.Column(Connector.db.Integer(), unique=True, primary_key=True)
    profile_key = Connector.db.Column(Connector.db.String(70), Connector.db.ForeignKey('profile.profile_key'))
    api_name = Connector.db.Column(Connector.db.String(30), Connector.db.ForeignKey('apis.name'))
    date_subscribed = Connector.db.Column(Connector.db.Date())

    def __init__(self, profile_key=None, api_name=None, date_subscribed=None):
        self.profile_key = profile_key
        self.api_name = api_name
        self.date_subscribed = date_subscribed

    def __repr__(self):
        return '<Sub {api_name} by {profile}'.format(api_name=self.api_name, profile=self.profile_key)

    def to_dict(self):
        return dict(
            id=self.sub_id,
            api_name=self.api_name,
            profile_key=self.profile_key,
            date_subscribed=str(self.date_subscribed)
        )
