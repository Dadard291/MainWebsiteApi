from datetime import datetime

from MainWebsiteApi.dal.repository.connector import Connector


class ProfileEntity(Connector.db.Model):
    __tablename__ = 'profile'
    profile_key = Connector.db.Column(Connector.db.String(70), unique=True, primary_key=True, default=1)
    username = Connector.db.Column(Connector.db.String(70), unique=True)
    hashed_password = Connector.db.Column(Connector.db.String(70))
    date_registered = Connector.db.Column(Connector.db.Date())
    date_connected = Connector.db.Column(Connector.db.Date())
    subscriptions = Connector.db.relationship('SubscriptionEntity', backref=__tablename__, lazy=True)

    def __init__(self, username=None,
                 hashed_password=None,
                 profile_key=None,
                 date_registered=datetime.now().date(),
                 date_connected=datetime.now().date()):
        self.username = username
        self.hashed_password = hashed_password
        self.profile_key = profile_key
        self.date_registered = date_registered
        self.date_connected = date_connected

    def __repr__(self):
        return '<User %r>' % self.username

    def to_dict(self):
        return dict(
            profile_key=self.profile_key,
            username=self.username,
            date_registered=str(self.date_registered),
            date_connected=str(self.date_connected)
        )
