import json

import requests

from MainWebsiteApi.common.common import get_current_date
from MainWebsiteApi.common.config_accessor import ConfigAccessor
from MainWebsiteApi.common.error_messages import *
from MainWebsiteApi.common.exceptions import DalException
from MainWebsiteApi.common.globals import *
from MainWebsiteApi.dal.models.api_catalog_model import ApiEntity
from MainWebsiteApi.dal.models.profile_model import ProfileEntity
from MainWebsiteApi.dal.repository.connector import Connector
from MainWebsiteApi.dto.api_dto import ApiDto


class ApiDal:

    def __init__(self):
        self.session = Connector.db.session
        self.config_slaves = ConfigAccessor.config_access('slaves')
        self.base_url = self.config_slaves['base_url']
        self.ping_endpoint = self.config_slaves['ping_endpoint']
        self.param_api_key = self.config_slaves['param_api_key']

    def get_query(self):
        return self.session.query(ApiEntity)

    def get_profile_user_query(self):
        return self.session.query(ProfileEntity)

    def check_root_user(self, profile_key):
        p = self.get_profile_user_query().filter_by(profile_key=profile_key).first()
        return p.username == ROOT_USERNAME

    def ping(self, api_name):
        try:
            api = self.get_query().filter_by(name=api_name).first()

            if api is None:
                raise DalException(API_NOT_EXISTS)

            # call the ping endpoint of the given API
            url = f'http://{MAIN_HOST}:{api.port}/{self.base_url}{self.ping_endpoint}'
            r = requests.get(url)
            return r.status_code == 200

        except Exception as e:
            self.session.rollback()
            return False

    def register_api(self, api_object: ApiDto, profile_key):
        try:
            if not self.check_root_user(profile_key):
                raise DalException(UNAUTHORIZED)

            n = self.get_query().filter_by(name=api_object.name).first()

            if n is not None:
                raise DalException(API_ALREADY_EXISTS)

            # check port not already used
            port_usage = self.get_query().filter_by(port=api_object.port).first()

            if port_usage is not None:
                raise DalException(PORT_ALREADY_USED)

            api = ApiEntity(
                api_object.name,
                api_object.description,
                api_object.port,
                api_object.creation_date,
                api_object.url_badge_build,
                api_object.url_badge_package,
                api_object.container_image_registry,
                api_object.long_description,
                api_object.api_documentation
            )
            self.session.add(api)
            self.session.commit()

            return api.to_dict()

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))

    def deregister_api(self, api_name, profile_key):
        try:
            if not self.check_root_user(profile_key):
                raise DalException(UNAUTHORIZED)

            api = self.get_query().filter_by(name=api_name).first()

            if api is None:
                raise DalException(API_NOT_EXISTS)

            self.session.delete(api)
            self.session.commit()

            return api.to_dict()

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))

    def get_api(self, api_name):
        try:
            # everybody can preview api
            api = self.get_query().filter_by(name=api_name).first()

            if api is None:
                raise DalException(API_NOT_EXISTS)

            res = api.to_dict()
            return res

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))

    def retrieve_long_description(self, api_name):
        try:
            api = self.get_query().filter_by(name=api_name).first()

            if api is None:
                raise DalException(API_NOT_EXISTS)

            readme_url = api.long_description
            res = str(requests.get(readme_url).content.decode('utf-8'))

            return res

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))


    def list_apis(self):
        try:
            # everybody can list
            apis = self.get_query().all()

            res_list = []
            for entity in apis:
                res_list.append(entity.overview_to_dict())

            return res_list

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))


