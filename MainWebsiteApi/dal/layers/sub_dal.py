import json
import logging

import requests

from MainWebsiteApi.common.common import get_current_date
from MainWebsiteApi.common.config_accessor import ConfigAccessor
from MainWebsiteApi.common.error_messages import *
from MainWebsiteApi.common.exceptions import DalException
from MainWebsiteApi.common.globals import ROOT_USERNAME, STATUS_OK, MAIN_HOST
from MainWebsiteApi.dal.models.api_catalog_model import ApiEntity, SubscriptionEntity
from MainWebsiteApi.dal.models.profile_model import ProfileEntity
from MainWebsiteApi.dal.repository.connector import Connector

logger = logging.getLogger(__name__)


class SubDal:

    def __init__(self):
        self.session = Connector.db.session
        self.config_slaves = ConfigAccessor.config_access('slaves')
        self.base_url = self.config_slaves['base_url']
        self.sub_endpoint = self.config_slaves['sub_endpoint']
        self.param_api_key = self.config_slaves['param_api_key']

    def get_query(self):
        return self.session.query(SubscriptionEntity)

    def get_profile_user_query(self):
        return self.session.query(ProfileEntity)

    def get_api_query(self):
        return self.session.query(ApiEntity)

    def check_api_exists(self, api_name):
        # check api exist
        a = self.get_api_query().filter_by(name=api_name).first()
        if a is None:
            raise DalException(API_NOT_EXISTS)

        return a

    def check_user_exists(self, profile_key):
        # check user exists
        p = self.get_profile_user_query().filter_by(profile_key=profile_key).first()
        if p is None:
            raise DalException(USER_NOT_EXISTS)

        return p

    def create_sub(self, api_name, profile_key):
        """
        Always call the slave endpoint FIRST, before interacting with db (checks slaves side)
        :param api_name:
        :param profile_key:
        :return:
        """
        try:
            api = self.check_api_exists(api_name)
            profile = self.check_user_exists(profile_key)

            check_sub = self.get_query().filter_by(profile_key=profile_key, api_name=api_name).first()
            if check_sub is not None:
                raise DalException(SUB_ALREADY_EXISTS)

            # call the subscribe endpoint of the slave API
            url = f'http://{MAIN_HOST}:{api.port}/{self.base_url}{self.sub_endpoint}'
            h = {"Authorization": profile_key}
            r = json.loads(requests.post(url, headers=h).content.decode())
            assert r['status'] == STATUS_OK, "{error}: {message}".format(error=SLAVE_ERROR, message=r['message'])

            sub_date = get_current_date()
            sub = SubscriptionEntity(profile_key, api_name, sub_date)

            self.session.add(sub)
            self.session.commit()

            return sub.to_dict()

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))

    def delete_sub(self, api_name, profile_key):
        """
        Always call the slave endpoint FIRST, before interacting with db (checks slaves side)
        :param api_name:
        :param profile_key:
        :return:
        """
        try:
            api = self.check_api_exists(api_name)
            profile = self.check_user_exists(profile_key)

            # call the subscribe endpoint of the slave API
            url = f'http://{MAIN_HOST}:{api.port}/{self.base_url}{self.sub_endpoint}'
            h = {"Authorization": profile_key}
            r = json.loads(requests.delete(url, headers=h).content.decode())
            assert r['status'] == STATUS_OK, "{error}: {message}".format(error=SLAVE_ERROR, message=r['message'])

            # if unsubscribe went well slave side, then delete it from db
            sub = self.get_query().filter_by(profile_key=profile_key, api_name=api_name).first()

            if sub is None:
                raise DalException(SUB_NOT_EXISTS)

            self.session.delete(sub)
            self.session.commit()

            return sub.to_dict()

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))

    def get_sub(self, api_name, profile_key):
        try:
            self.check_api_exists(api_name)
            self.check_user_exists(profile_key)

            sub = self.get_query().filter_by(profile_key=profile_key, api_name=api_name).first()
            if sub is None:
                return {
                    'is_subscribed': 'false',
                }

            return {
                'is_subscribed': 'true',
                'subscription': sub.to_dict()
            }

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))

    def list_subs(self, profile_key):
        try:
            self.check_user_exists(profile_key)

            sub = self.get_query().filter_by(profile_key=profile_key).all()

            res_list = []
            for entity in sub:
                res_list.append(entity.to_dict())

            return res_list

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))
