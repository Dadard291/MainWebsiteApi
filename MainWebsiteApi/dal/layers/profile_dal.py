import datetime
import os

import jwt

from MainWebsiteApi.common.common import check_password, get_current_date
from MainWebsiteApi.common.error_messages import *
from MainWebsiteApi.common.exceptions import DalException
from MainWebsiteApi.dal.models.api_catalog_model import SubscriptionEntity
from MainWebsiteApi.dal.models.profile_model import ProfileEntity
from MainWebsiteApi.dal.repository.connector import Connector


class ProfileDal:

    def __init__(self):
        self.session = Connector.db.session

    def get_query(self):
        return self.session.query(ProfileEntity)

    def get_sub_query(self):
        return self.session.query(SubscriptionEntity)

    def create_profile(self, username, hashed_password, profile_key):
        try:
            p = self.get_query().filter_by(username=username).first()

            if p is not None:
                raise DalException(USER_ALREADY_EXISTS)

            now = get_current_date()
            p = ProfileEntity(username, hashed_password, profile_key, date_registered=now, date_connected=now)
            self.session.add(p)
            self.session.commit()
            return p.to_dict()

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))

    def delete_profile(self, username, encoded_password):
        try:
            p = self.get_query().filter_by(username=username).first()

            if p is None:
                raise DalException(USER_NOT_EXISTS)

            if not check_password(encoded_password, p.hashed_password):
                raise DalException(AUTHENTICATION_ERROR)

            # check remaining subs
            subs = self.get_sub_query().filter_by(profile_key=p.profile_key).all()
            if len(subs) != 0:
                raise DalException(SUB_LIST_NOT_EMPTY)

            self.session.delete(p)
            self.session.commit()
            return p.to_dict()

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))

    def get_profile(self, username, encoded_password):
        try:
            p = self.get_query().filter_by(username=username).first()

            if p is None:
                raise DalException(USER_NOT_EXISTS)

            if not check_password(encoded_password, p.hashed_password):
                raise DalException(AUTHENTICATION_ERROR)

            now = get_current_date()
            p.date_connected = now
            self.session.commit()

            return p.to_dict()

        except Exception as e:
            raise DalException(str(e))

    def get_profile_infos(self, profile_key):
        try:
            p = self.get_query().filter_by(profile_key=profile_key).first()

            if p is None:
                raise DalException(USER_NOT_EXISTS)

            return p.to_dict()

        except Exception as e:
            raise DalException(str(e))

    def create_jwt(self, profile_key):
        try:
            p = self.get_query().filter_by(profile_key=profile_key).first()

            if p is None:
                raise DalException(USER_NOT_EXISTS)

            # retrieve the secret key from env
            jwt_secret = os.environ['JWT_SECRET']
            algo = 'HS256'
            expiration_time = 12  # hours number of validity
            jwt_encoded = jwt.encode(
                {
                    'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=expiration_time),
                    'profile_key': p.profile_key
                },
                jwt_secret,
                algorithm=algo
            )

            return jwt_encoded.decode()

        except Exception as e:
            raise DalException(str(e))

    def update_profile(self, username, encoded_password, new_hashed_password):
        try:
            p = self.get_query().filter_by(username=username).first()

            if p is None:
                raise DalException(USER_NOT_EXISTS)

            if not check_password(encoded_password, p.hashed_password):
                raise DalException(AUTHENTICATION_ERROR)

            p.hashed_password = new_hashed_password
            self.session.commit()

            return p.to_dict()

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))

    def regenerate_profile_key(self, username, encoded_password, new_profile_key):
        try:
            p = self.get_query().filter_by(username=username).first()

            if p is None:
                raise DalException(USER_NOT_EXISTS)

            if not check_password(encoded_password, p.hashed_password):
                raise DalException(AUTHENTICATION_ERROR)

            # check remaining subs
            subs = self.get_sub_query().filter_by(profile_key=p.profile_key).all()
            if len(subs) != 0:
                raise DalException(SUB_LIST_NOT_EMPTY)

            p.profile_key = new_profile_key
            self.session.commit()

            return p.to_dict()

        except Exception as e:
            self.session.rollback()
            raise DalException(str(e))



