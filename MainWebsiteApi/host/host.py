from flask import Blueprint
from flask_restplus import Api, fields

# init the REST API nested into the flask application with the blueprint
from MainWebsiteApi.common.config_accessor import ConfigAccessor

config = ConfigAccessor.config_access('api')


api_v1 = Blueprint('api', __name__, url_prefix=config['url_prefix'])

api = Api(
    api_v1,
    title=config['title'],
    doc=config['url_doc'],
    version=config['version'],
    description=config['description'],
    contact_email=config['contact'],
    license=config['license'],
    license_url=config['license_url']
)

profile_authorization = {
    "login_password": {
        "type": "apiKey",
        "in": "header",
        "name": "Authorization"
    }
}

api_authorization = {
    "api_key": {
        "type": "apiKey",
        "in": "header",
        "name": "Authorization"
    }
}

api_model = api.model('Api', {
    'api_name': fields.String(required=True, description="API\'s name"),
    'description': fields.String(required=True, descripion='API\'s description'),
    'port': fields.Integer(required=True, description='the port in use'),
    'url_badge_build': fields.String(required=True, descripion='API\'s build status'),
    'url_badge_package': fields.String(required=True, descripion='API\'s package version'),
    'container_image_registry': fields.String(required=True, descripion='API\'s image registry'),
    'long_description': fields.String(required=True, descripion='API\'s readme url'),
    'api_documentation': fields.String(required=True, descripion='API\'s documentation url'),
})

sub_model = api.model('Sub', {
    'api_name': fields.String(required=True, description="APIS\'s name"),
    'profile_key': fields.String(required=True, description="the user\'s key")
})

# define the namespaces
profile_ns = api.namespace('profile', description='profile management', authorizations=profile_authorization)
api_ns = api.namespace('api', description='api management', authorizations=api_authorization)
sub_ns = api.namespace('sub', description='subs management', authorizations=api_authorization)
misc_ns = api.namespace('misc', description='others features')

# execute the controllers files, in order to register the routes to the namespaces
from MainWebsiteApi.controllers.profile_controller import *
from MainWebsiteApi.controllers.api_controller import *
from MainWebsiteApi.controllers.sub_controller import *
from MainWebsiteApi.controllers.misc_controller import *

