from MainWebsiteApi.common.common import get_current_date


class ApiDto(object):
    def __init__(self, name, description, port, url_badge_build,
                 url_badge_package, container_image_registry,
                 long_description, api_documentation):
        self.name = name
        self.description = description
        self.port = port
        self.creation_date = get_current_date()
        self.url_badge_build = url_badge_build
        self.url_badge_package = url_badge_package
        self.container_image_registry = container_image_registry
        self.long_description = long_description
        self.api_documentation = api_documentation
