from flask import request
from flask_restplus import Resource

from MainWebsiteApi.host.host import profile_ns
from MainWebsiteApi.common.common import format_error, format_response, check_query_parameter
from MainWebsiteApi.common.error_messages import MISSING_PARAMETER
from MainWebsiteApi.common.exceptions import BusinessException
from MainWebsiteApi.manager.profile_manager import ProfileManager


def get_credentials(authorization):
    if authorization is None:
        return None, None

    username = authorization.username.strip()
    password = authorization.password.strip()

    return username, password


def get_profile_key_from_request(l_request):
    return l_request.headers.get("Authorization")


@profile_ns.route('/connect')
class ProfileConnect(Resource):
    manager = ProfileManager()

    @profile_ns.doc(
        security="api_key"
    )
    def get(self):
        try:
            profile_key = get_profile_key_from_request(request)
            if not check_query_parameter(profile_key):
                return format_error(MISSING_PARAMETER)

            jwt = self.manager.create_jwt(profile_key)
            return format_response('jwt created', jwt)
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))


@profile_ns.route('/regenerate')
class ProfileRegen(Resource):
    manager = ProfileManager()

    @profile_ns.doc(
        security="login_password"
    )
    def put(self):
        try:
            # not working (yet)
            username, password = get_credentials(request.authorization)
            if not check_query_parameter(username, password):
                return format_error(MISSING_PARAMETER)

            profile = self.manager.regenerate_profile_key(username, password)
            return format_response('key regenerated', profile)
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))


@profile_ns.route('/infos')
class ProfileInfos(Resource):
    manager = ProfileManager()

    @profile_ns.doc(
        security="api_key"
    )
    def get(self):
        try:
            profile_key = get_profile_key_from_request(request)
            if not check_query_parameter(profile_key):
                return format_error(MISSING_PARAMETER)

            profile = self.manager.get_profile_infos(profile_key)
            return format_response('profile retrieved', profile)
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error: {}'.format(e))


@profile_ns.route('/')
class Profile(Resource):
    manager = ProfileManager()

    @profile_ns.doc(
        security="login_password"
    )
    def get(self):
        try:
            username, password = get_credentials(request.authorization)
            if not check_query_parameter(username, password):
                return format_error(MISSING_PARAMETER)

            profile = self.manager.get_profile(username, password)
            return format_response('key retrieved', profile)
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error: {}'.format(e))

    @profile_ns.doc(
        security="login_password"
    )
    def post(self):
        try:
            username, password = get_credentials(request.authorization)
            if not check_query_parameter(username, password):
                return format_error(MISSING_PARAMETER)

            profile = self.manager.create_profile(username, password)
            return format_response('account created', profile)

        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))

    @profile_ns.doc(
        security="login_password"
    )
    def delete(self):
        try:
            username, password = get_credentials(request.authorization)
            if not check_query_parameter(username, password):
                return format_error(MISSING_PARAMETER)

            profile = self.manager.delete_profile(username, password)
            return format_response('account deleted', profile)
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))

    @profile_ns.doc(
        security="login_password",
        params={
            'new_password': 'the new password'
        }
    )
    def put(self):
        try:
            username, password = get_credentials(request.authorization)
            new_password = request.args.get('new_password')
            if not check_query_parameter(username, password, new_password):
                return format_error(MISSING_PARAMETER)

            profile = self.manager.update_profile(username, password, new_password)
            return format_response('password updated', profile)
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))
