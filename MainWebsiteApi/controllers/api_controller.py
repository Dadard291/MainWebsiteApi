from flask import request
from flask_restplus import Resource

from MainWebsiteApi.common.common import check_query_parameter, format_error, format_response
from MainWebsiteApi.common.error_messages import MISSING_PARAMETER
from MainWebsiteApi.common.exceptions import BusinessException
from MainWebsiteApi.dto.api_dto import ApiDto
from MainWebsiteApi.host.host import api_ns, api_model
from MainWebsiteApi.manager.api_manager import ApiManager

manager = ApiManager()


def get_profile_key_from_request(l_request):
    return l_request.headers.get("Authorization")


@api_ns.route('/<api_name>/ping')
class ApiPing(Resource):
    def get(self, api_name):
        try:
            status = manager.ping(api_name)
            return format_response('apis ping', {'api_name': api_name, 'status': status})

        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))


@api_ns.route('/list')
class ApiList(Resource):
    # no security, everybody can access the catalog
    def get(self):
        try:
            api_list = manager.list_api()
            return format_response('apis listed', api_list)

        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))


@api_ns.route('/<api_name>')
class Api(Resource):

    def get(self, api_name):
        try:
            if not check_query_parameter(api_name):
                return format_error(MISSING_PARAMETER)

            api = manager.get_api(api_name)
            return format_response('api {name} retrieved'.format(name=api_name), api)

        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))


@api_ns.route('/<api_name>/longDescription')
class ApiLongDescription(Resource):

    def get(self, api_name):
        try:
            if not check_query_parameter(api_name):
                return format_error(MISSING_PARAMETER)

            ld = manager.retrieve_long_description(api_name)
            return format_response('api {name} long description retrieved'.format(name=api_name), ld)

        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))


@api_ns.route('/private')
@api_ns.doc(
    security='api_key',
)
class ApiManagement(Resource):

    @api_ns.doc(
        params={
            'api_name': 'the API\'s name'
        }
    )
    def delete(self):
        try:
            profile_key = get_profile_key_from_request(request)
            api_name = request.args.get('api_name')
            if not check_query_parameter(profile_key, api_name):
                return format_error(MISSING_PARAMETER)

            api = manager.deregister_api(api_name, profile_key)
            return format_response('api {name} deleted'.format(name=api_name), api)

        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))

    @api_ns.expect(api_model)
    def post(self):
        try:
            payload = request.json

            api_name = payload['api_name']
            description = payload['description']
            port = payload['port']
            url_badge_build = payload.get('url_badge_build')
            url_badge_package = payload.get('url_badge_package')
            container_image_registry = payload.get('container_image_registry')
            long_description = payload.get('long_description')
            api_documentation = payload.get('api_documentation')

            profile_key = get_profile_key_from_request(request)

            if not check_query_parameter(
                    profile_key,
                    api_name,
                    description,
                    port,
                    url_badge_build,
                    url_badge_package,
                    container_image_registry,
                    long_description,
                    api_documentation
            ):
                return format_error(MISSING_PARAMETER)

            # mapping
            api_object = ApiDto(
                api_name,
                description,
                port,
                url_badge_build,
                url_badge_package,
                container_image_registry,
                long_description,
                api_documentation
            )

            api = manager.register_api(api_object, profile_key)
            return format_response('api {name} created'.format(name=api_name), api)

        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))
