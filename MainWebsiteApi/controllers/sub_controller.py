from flask import request
from flask_restplus import Resource

from MainWebsiteApi.common.common import check_query_parameter, format_error, format_response
from MainWebsiteApi.common.error_messages import MISSING_PARAMETER
from MainWebsiteApi.common.exceptions import BusinessException
from MainWebsiteApi.host.host import sub_ns, sub_model
from MainWebsiteApi.manager.sub_manager import SubManager


def get_profile_key_from_request(l_request):
    return l_request.headers.get("Authorization")


@sub_ns.route('/list')
@sub_ns.doc(
    security='api_key'
)
class SubList(Resource):
    manager = SubManager()

    def get(self):
        try:
            profile_key = get_profile_key_from_request(request)

            if not check_query_parameter(profile_key):
                return format_error(MISSING_PARAMETER)

            sub_list = self.manager.list_subs(profile_key)
            return format_response('subs listed', sub_list)

        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))


@sub_ns.route('/')
@sub_ns.doc(
    security='api_key'
)
class Sub(Resource):
    manager = SubManager()

    @sub_ns.doc(
        params={
            'api_name': 'the API\'s name',
        }
    )
    def get(self):
        try:
            profile_key = get_profile_key_from_request(request)
            api_name = request.args.get('api_name')

            if not check_query_parameter(profile_key, api_name):
                return format_error(MISSING_PARAMETER)

            sub = self.manager.get_sub(api_name, profile_key)
            return format_response('sub retrieved'.format(name=api_name), sub)

        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))

    @sub_ns.doc(
        params={
            'api_name': 'the API\'s name',
        }
    )
    def post(self):
        try:
            profile_key = get_profile_key_from_request(request)
            api_name = request.args.get('api_name')

            if not check_query_parameter(profile_key, api_name):
                return format_error(MISSING_PARAMETER)

            sub = self.manager.create_sub(api_name, profile_key)
            return format_response('sub created'.format(name=api_name), sub)

        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))

    @sub_ns.doc(
        params={
            'api_name': 'the API\'s name',
        }
    )
    def delete(self):
        try:
            profile_key = get_profile_key_from_request(request)
            api_name = request.args.get('api_name')

            if not check_query_parameter(profile_key, api_name):
                return format_error(MISSING_PARAMETER)

            sub = self.manager.delete_sub(api_name, profile_key)
            return format_response('sub deleted'.format(name=api_name), sub)
        except BusinessException as be:
            return format_error(be.msg)
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))
