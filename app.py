from flask import Flask
from flask_cors import CORS
import os
import logging.config


from MainWebsiteApi.dal.repository.connector import Connector, logger

# init the main flask application
flask_app = Flask(__name__)

# initiate the database connector BEFORE the other modules import, so the connector object is correctly set
Connector.init_db(flask_app)

from MainWebsiteApi.common.config_accessor import ConfigAccessor
from MainWebsiteApi.host.host import api_v1
from waitress import serve

logger.debug('starting app')

# register the api with the blueprint
flask_app.register_blueprint(api_v1)

# avoid cors policy restriction when request received
CORS(flask_app)

config = ConfigAccessor.config_access('host')
host = config['host']
port = config['port']

# RUN, U FOOLS
if __name__ == '__main__':
    if os.environ["API_ENV"] == "PROD":
        # fuck logs, at least this works
        serve(flask_app, listen="{host}:{port}".format(host=host, port=port))
    else:
        # serve(flask_app, listen="{host}:{port}".format(host=host, port=port))
        flask_app.run()  # DEV MODE ONLY
